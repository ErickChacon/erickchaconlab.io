# Statistical Issues Index

## [Introduction to Statistics](statistics/_index.Rmd)

- Probability
- Likelihood
- Confidence Intervals
- Statistical Methods
- Classic Statistics
- Bayesian statistics
- Comparing classic and Bayesian statistics
- Causality, Confounding

## [Statistical Inference](inference/_index.Rmd)

- *Classical Inference Techniques*
  - Maximum likelihood estimation
    - [Expectation maximization]($blogdir/_inference/expectation-maximization.md)
    - Iteratively reweighted least squares
  - Restricted maximum likelihood estimation
- *Bayesian Inference Techniques*
  - Rejection sampling
  - Importance sampling
  - Markov chain Monte Carlo
    - Gibbs sampling
    - [Metropolis Hasting](Metropolis Hasting)
  - Laplace Approximation
  - Integrated nested Laplace approximation
  - Variational Bayes

## [Modelling](sfdfd/_index.Rmd)

- *Introduction to Modelling*
  - Classical statistics
  - Bayesian statistics
  - Comparing classical and Bayesian statistics
- *Common Models*
  - Linear Gaussian models
  - Generalized linear models
  - Mixed-effects models
  - [Additive models](Additive models)
  - Quantile Regression
- *Spatio-temporal Models*
  - Based-model geostatistics
    - Low-rank models
    - Nearest neighbor Gaussian process
  - Dynamic models
    - Dynamic linear models
- *More General Models*
  - Location, scale and shape models
  - Structured additive regression models

## [Computational Methods](sfdfd/_index.Rmd)

## [Mathematics](mathematics/_index.Rmd)

- *Linear Algebra*
  - [Linear Transformation and Change of Basis](mathematics/linear-algebra/10-linear-transformation.Rmd)
  - [Eigenvalues](mathematics/linear-algebra/20-eigenvalues.Rmd)

## [Programming](sfdfd/_index.Rmd)

## [Blog](Blog)





See which plugin has set the mapping `gf`.

```
:verbose map gf
```
