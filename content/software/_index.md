---
title: Software Development
type: software
---

I prefer to work with the GNU/Linux operating system and free programming
languages like R, Python, C++, Julia, LaTex and Markdown. Below you will find
projects I have been working on. My **Gitlab** and **Github** profiles can be
found on the footer of this web page.
