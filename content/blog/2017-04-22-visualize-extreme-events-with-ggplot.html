---
title: "Visualizing extreme events with ggplot2"
date: 2017-04-22 23:27:59.00 +01:00
categories:
- data visualization
tags:
- ggplot2
- tidyverse
- R
---



<p>When working with time series, sometimes, it is desired to highlight some events with some particular pattern.</p>
<p>For example, highlight periods of time where the variable on interest exceed certain threshold or conversely. With this in mind, I extended an <code>stat</code> of <code>ggplot2</code> to allow easy visualization of those events. A will show how to use this <code>stat</code> using a simulated time series with exponential correlation function.</p>
<div id="required-packages" class="section level2">
<h2>Required packages</h2>
<p>I will use the <code>tidyverse</code> collection of packages, <code>lubridate</code> and my package <code>day2day</code>. If you want to install <code>day2day</code>, you can get it using the command <code>devtools::install_github(&quot;erickchacon/day2day&quot;)</code>.</p>
<pre class="r"><code>library(tidyverse)</code></pre>
<pre><code>## Loading tidyverse: ggplot2
## Loading tidyverse: tibble
## Loading tidyverse: tidyr
## Loading tidyverse: readr
## Loading tidyverse: purrr
## Loading tidyverse: dplyr</code></pre>
<pre><code>## Conflicts with tidy packages ----------------------------------------------</code></pre>
<pre><code>## filter(): dplyr, stats
## lag():    dplyr, stats</code></pre>
<pre class="r"><code>library(lubridate)</code></pre>
<pre><code>## 
## Attaching package: &#39;lubridate&#39;</code></pre>
<pre><code>## The following object is masked from &#39;package:base&#39;:
## 
##     date</code></pre>
<pre class="r"><code>library(day2day)</code></pre>
</div>
<div id="simulating-correlated-time-series" class="section level2">
<h2>Simulating correlated time series</h2>
<p>I use a exponential correlation function <span class="math inline">\(c(d) = \exp(-d/\phi)\)</span> to represent the correlation between two random variables with temporal distance of <span class="math inline">\(d\)</span>. Then, in order to simulate <span class="math inline">\(Y \sim N(\mu, \mathbf{\Sigma})\)</span>, I use the Cholesky decomposition <span class="math inline">\(\mathbf{\Sigma} = \mathbf{L}\mathbf{L}^\intercal\)</span>. Defining <span class="math inline">\(Z \sim N(0, I)\)</span>, it can be shown that <span class="math inline">\(\mathbf{L}Z \sim N(\mu, \mathbf{L}U = \Sigma)\)</span>. Hence, I just have to simulate <span class="math inline">\(Z\)</span>, <code>rnorm(n)</code>, and compute <span class="math inline">\(\mathbf{L}Z\)</span>.</p>
<pre class="r"><code>n &lt;- 300
time &lt;- seq(1, n, 3)
temporal_dist &lt;- as.matrix(dist(time))
cov_matrix &lt;- exp(-temporal_dist/7)
upper_chol &lt;- chol(cov_matrix)
set.seed(1)
precipitation &lt;- t(upper_chol) %*% rnorm(length(time))
time &lt;- ymd(&quot;05-01-01&quot;) + weeks(time)
data &lt;- data.frame(time, precipitation)</code></pre>
<p>Then, the resulting data frame contains the time and the standardized precipitation under analysis as shown below. Note that most of the values are between -2 and 2.</p>
<pre class="r"><code>ggplot(data, aes(time, precipitation)) + geom_line()</code></pre>
<p><img src="/post/2017-04-22-visualize-extreme-events-with-ggplot_files/figure-html/unnamed-chunk-3-1.png" width="1400" /></p>
</div>
<div id="visualizing-positive-and-negative-events" class="section level2">
<h2>Visualizing positive and negative events</h2>
<p>To start, I would like to highlight the difference between events that are constantly positive or negative. So, I made a try using <code>geom_area</code>.</p>
<pre class="r"><code>ggplot(data, aes(time, precipitation)) +
  geom_line() +
  geom_point(color = 2, alpha = 0.3) +
  geom_area(alpha = 0.3)</code></pre>
<p><img src="/post/2017-04-22-visualize-extreme-events-with-ggplot_files/figure-html/unnamed-chunk-4-1.png" width="1400" /></p>
<p>It makes the work, but the quality of the <code>geom_area</code> when the series change from positive to negative, or conversely, is not good. You can see that there is a difference between the <code>geom_line</code> and <code>geom_area</code> around these transitions. So, I created a <code>stat_events</code> that make the same thing as <code>geom_area</code>, but with better quality. Look in the next image how the <code>lines</code> and <code>areas</code> match perfectly.You can see that code to create <code>stat_events</code> in <a href="https://github.com/ErickChacon/day2day/blob/5967cb0daa4e1237d6bcf73a0c3406e33bcf08f3/R/gg-adds.R#L143-L192">my day2day package</a>.</p>
<pre class="r"><code>ggplot(data, aes(time, precipitation)) +
  geom_line() +
  geom_point(color = 2, alpha = 0.3) +
  stat_events(alpha = 0.3)</code></pre>
<p><img src="/post/2017-04-22-visualize-extreme-events-with-ggplot_files/figure-html/unnamed-chunk-5-1.png" width="1400" /></p>
<p>The geometry <code>area</code> only allow you to make a <code>ribbon</code> with a reference point of zero. However, maybe, you would like to differentiate two events using another reference point (e.g. <span class="math inline">\(-0.5\)</span> or <span class="math inline">\(0.5\)</span>). This is allow in <code>stat_events</code> by defining the threshold of interest. In the next image, I have used <code>threshold = 0.5</code>.</p>
<pre class="r"><code>ggplot(data, aes(time, precipitation)) +
  geom_line() +
  geom_point(color = 2, alpha = 0.3) +
  stat_events(threshold = 0.5, alpha = 0.3)</code></pre>
<p><img src="/post/2017-04-22-visualize-extreme-events-with-ggplot_files/figure-html/unnamed-chunk-6-1.png" width="1400" /></p>
</div>
<div id="differentiating-events" class="section level2">
<h2>Differentiating events</h2>
<p>Now it comes the fun part, the main reason I created this <code>stat</code> is because I wanted to differentiate two different events by coloring them and probably with a name for the legend. My first thought to highlight particular events (e.g. only positives) was using <code>subset</code> on <code>geom_area</code>.</p>
<pre class="r"><code>ggplot(data, aes(time, precipitation)) +
  geom_line() +
  geom_area(data = subset(data, precipitation &gt; 0), alpha = 0.3)</code></pre>
<p><img src="/post/2017-04-22-visualize-extreme-events-with-ggplot_files/figure-html/unnamed-chunk-7-1.png" width="1400" /></p>
<p>It did not give me good results, because it just jump from one positive event to the next positive event, this jump make the polygons look different from the original time series. For this reason, I added the <code>event</code> aesthetics to <code>stat_events</code> such as it indicates the event of interest. It should be numeric variable taking <span class="math inline">\(1\)</span> (event), <span class="math inline">\(0\)</span> (no event) and <code>NA</code>. With this argument, we can easily identify and draw events exceeding or not certain threshold. Let’s assume that flood events are detected when the standardized precipitation is greater than <span class="math inline">\(&gt;0.5\)</span> and droughts when it is lower than <span class="math inline">\(&lt;-0.5\)</span>. They can be highlighted as follows.</p>
<pre class="r"><code>ggplot(data, aes(time, precipitation)) +
  geom_line() +
  stat_events(aes(event = I(1 * (precipitation &gt; 0.5)), fill = &quot;flood&quot;),
              threshold = 0.5, alpha = 0.3) +
  stat_events(aes(event = I(1 * (precipitation &lt; -0.5)), fill = &quot;drought&quot;),
              threshold = - 0.5, alpha = 0.3)</code></pre>
<pre><code>## Warning: Ignoring unknown aesthetics: event

## Warning: Ignoring unknown aesthetics: event</code></pre>
<p><img src="/post/2017-04-22-visualize-extreme-events-with-ggplot_files/figure-html/unnamed-chunk-8-1.png" width="1400" /></p>
</div>
<div id="highlighting-floods-and-droughts" class="section level2">
<h2>Highlighting floods and droughts</h2>
<p>McKee defined a drought as a period of time where the standardized precipitation index (SPI) is continuously negative falling down than <span class="math inline">\(-1\)</span> at least one time. Based on this definition, I created a function <code>find_flood_drought</code> to detect extreme events using the SPI. You can view the code <a href="https://github.com/ErickChacon/day2day/blob/347d0774d89eed5c020834f8924ea9e94e17cbd7/R/spi-rain.R#L111-L132">here</a>, Then a visualization of extreme events can be done using <code>find_flood_drought</code> and <code>stat_events</code> as follows. Note that the event is used as same as before, indicating with a value equals to <span class="math inline">\(1\)</span> that we want to highlight that corresponding value of the precipitation.</p>
<pre class="r"><code>data &lt;- data %&gt;% mutate(
  extremes = find_flood_drought(precipitation),
  floods = 1 * (extremes == &quot;flood&quot;),
  droughts = 1 * (extremes == &quot;drought&quot;))

ggplot(data, aes(time, precipitation)) +
  geom_line() +
  stat_events(aes(event = floods, fill = &quot;flood&quot;), alpha = 0.3) +
  stat_events(aes(event = droughts, fill = &quot;drought&quot;), alpha = 0.3)</code></pre>
<pre><code>## Warning: Ignoring unknown aesthetics: event

## Warning: Ignoring unknown aesthetics: event</code></pre>
<p><img src="/post/2017-04-22-visualize-extreme-events-with-ggplot_files/figure-html/unnamed-chunk-9-1.png" width="1400" /></p>
<p>I hope you have enjoyed this post about visualizing events in a time series and if you are asking yourself how I created the <code>stat_events</code> functions, take a view on this tutorial about <a href="http://ggplot2.tidyverse.org/articles/extending-ggplot2.html">extending ggplot2</a>.</p>
</div>
