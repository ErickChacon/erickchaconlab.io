---
categories:
  - Example
tags:
  - Markdown
image: spp.png
weight: 15
references:
- id: ChaconMontalvan2015
  type: thesis
  author:
  - family: Chacon Montalvan
    given: Erick A.
  issued:
  - year: '2015'
  title: '<span class="nocase">Spatio-Temporal Modelling of Vector-borne Diseases
    in the Brazilian Amazon: A View on Dengue and Malaria Burden</span>'
  publisher: Lancaster University
  page: '46'
  genre: PhD thesis
  abstract: Vector-borne diseases are a world concern, representing 17% of all infectious
    dis- eases and more than 1 million deaths annually. In particular, dengue and
    malaria, trans- mitted by mosquitoes, are the most alarming vector-borne diseases
    because the former is the mosquito-borne viral disease that had the highest incidence
    growth in the last 50 years (30-fold) and the latter has the highest mortality
    incidence with an estimate of 627 thousand deaths in 2012. Predicting the incidence
    of these diseases is an important step in improving control programmes in order
    to prevent outbreaks with an efficient distribution of logistics and human resources
    to the affected zones within a reasonable time; however, the risk factors that
    determined the incidence are not fully understood. In order to deter- mine the
    main risk factors affecting malaria and dengue incidence in the Brazilian Amazon
    between 2006-2013, Bayesian hierarchical latent Gaussian models were used through
    the integrated nested Laplace approximation (INLA) inference approach. The area
    of study covers 310 municipalities of 6 Federative Units and the considered factors
    include climatic and socio-economic variables in space, time and space-time domains.
    It has been showed that the Poisson distribution is not adequate for the observed
    data suggesting the use of the Negative Binomial distribution. Then, the Besag-York-Molliè
    (BYM) model in the spatial and spatio-temporal scale outperformed the Negative
    Binomial generalized linear model thanks to the inclusion of unstructured and
    structured random effects. The main findings confirmed that the temperature, precipitation
    and the Oceanic Niño Index were highly associated with dengue risk and that the
    urbanization, measured through the pop- ulation density, is a main risk factor
    for both dengue and malaria. Finally, the discussion and disadvantages of the
    BYM model and the INLA inference approach along with possi- ble competitor models
    are presented.
  keyword: Bayesian Hierarquical Modelling,Dengue, INLA ,Latent Gaussian models,Malaria,Spatial
    models,Spatio-temporal models.
---
