---
categories:
  - Example
tags:
  - Markdown
image: spp.png
weight: 5
references:
- id: ChaconMontalvan2014b
  type: article-journal
  author:
  - family: Chacon Montalvan
    given: Erick A.
  - family: Romero
    given: Vilma
  - family: Quispe
    given: Luisa
  - family: Camero
    given: Jose
  issued:
  - year: '2014'
  title: <span class="nocase">Methodology for Estimating Process Capability Indices
    in Non-normal Data</span>
  container-title: TECNIA
  page: '43-52'
  volume: '24'
  issue: '1'
  abstract: Globalization has intensified competition in many markets. To remain competitive,
    the companies look for satisfying the needs of customers by meeting market requi
    rements. In this context, Process Capability Indices (PCI) play a crucial role
    in assessing the quality of processes. In the case of non - normal data there
    are two general approaches based on transformations (Box - Cox and Johnson Transformation)
    and Percenti les (Pearson’s and Burr’s Distribution Systems). However, previous
    studies on the comparison of these methods show different conclusions, and thus
    arises the need to clarify the differences between these methods to implement
    a proper estimation of these in dices. In this paper , a simulation study is made
    in order to compare the above methods and to propose a n appropriate methodology
    for estimat ing the PCI in non - normal data. Furthermore, it is conclud ed that
    the best method used depends on the type of distribution, the asymmetry level
    of the distribution and the ICP value.
  keyword: Approximation to frequency distributions,Data transformations,Normality,Process
    capability indices,Simulation
  URL: http://tecnia.uni.edu.pe/site/DataTecnia/24/TECNIA24201405.pdf
---
