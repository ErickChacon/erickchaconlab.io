---
title: Spatio-temporal Models to Evaluate the Effect of Extreme Hydro-climatic Events on Birth Weight
date: '2017-12-18'
categories:
  - gamlss
tags:
  - extreme events
image: spp.png
event: 10th International Conference of the ERCIM WG on Computational and Methodological Statistics
location: University of London
---

Newborn weight is used as an indicator of population health because it is related to long-term outcomes in education, income and disability.  There are a number of existing studies that have been conducted to identify the major determinants of birth weight; in our study, we consider how the environment can impact on newborn health outcomes. In particular, the goal of the study is to evaluate the effects of exposure to floods and droughts during pregnancy on birth weight in roadless cities of the Brazilian Amazonas state. In our analysis, we make use of generalized additive models for location, scale and shape and divide the process into two stages. First, we propose a new model-based standardized precipitation index to quantify the magnitude of floods and droughts. Then, this index is used as covariate to evaluate the effects of exposure to extreme hydro-climatic events. Our results suggest: birth weight has a seasonal behaviour with respect to river levels; there is a global negative trend on birth weight; there exist vulnerable groups; and that exposure to extreme hydro-climatic events can have a negative impact on birth weight.

